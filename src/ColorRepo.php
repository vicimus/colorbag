<?php

namespace Vicimus\ColorBag;

use Symfony\Component\Yaml\Yaml;

/**
 * Holds information on color names
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class ColorRepo
{
    /**
     * Store the color information
     *
     * @var string[]
     */
    protected $dictionary = null;

    /**
     * Store hex totals with hexs
     *
     * @return string[]
     */
    protected $index = [];

    /**
     * Holds the singleton instance of the repo
     *
     * @var ColorRepo
     */
    private static $instance = null;

    /**
     * Enforce a singleton
     */
    protected function __construct()
    {
        //
    }

    /**
     * Get the singleton instance of the repo
     *
     * @return ColorRepo
     */
    public static function repo()
    {
        if (!self::$instance) {
            self::$instance = new ColorRepo;
        }

        return self::$instance;
    }

    /**
     * Find a name for a color based on hex
     *
     * @param string $hex The hex code of the color
     *
     * @return string
     */
    public function name($hex)
    {
        if (!$this->dictionary) {
            $this->parse();
        }

        if (array_key_exists($hex, $this->dictionary)) {
            return strtolower($this->dictionary[$hex]);
        }

        return null;
    }

    /**
     * Create a color instance form a name
     *
     * @param string $name The name of the color to make
     *
     * @return Color
     */
    public function make($name)
    {
        if (!$this->dictionary) {
            $this->parse();
        }

        $hex = array_search(ucwords(strtolower($name)), $this->dictionary);
        if (!$hex) {
            throw new \InvalidArgumentException(
                'Specified name ['.ucwords(strtolower($name)).'] could not be found'
            );
        }

        return new Color($hex);
    }

    /**
     * Parse the yml file
     *
     * @return ColorRepo
     */
    protected function parse()
    {
        $yml = Yaml::parse(file_get_contents(__DIR__.'/Colors.yml'));
        $this->dictionary = $yml;
        return $this;
    }

    /**
     * Parse the yml names but organize it in a way to allow duplicate hex
     * values, to allow for multiple names for the same hex code.
     *
     * @return ColorRepo
     */
    protected function parseNames()
    {
        $yml = Yaml::parse(file_get_contents(__DIR__.'/Colors.yml'));
    }
}
