<?php

namespace Vicimus\ColorBag;

/**
 * Represents a single color selected from the ColorBag
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class Color
{
    /**
     * The value of Red
     *
     * @var integer
     */
    protected $red;

    /**
     * The value of blue
     *
     * @var integer
     */
    protected $blue;

    /**
     * The value of green
     *
     * @var integer
     */
    protected $green;

    /**
     * The alpha value
     *
     * @var integer
     */
    protected $alpha = 1;

    /**
     * Default output of the toString method
     *
     * @var integer
     */
    protected $mode = 2;

    /**
     * Construct a new color from an rgba value
     *
     * @param mixed   $r    RED (or full HEX code with #)
     * @param integer $g    GREEN
     * @param integer $b    BLUE
     * @param integer $a    ALPHA
     * @param integer $mode 1 = HEX, 2 = RGBA
     */
    public function __construct($r, $g = null, $b = null, $a = 1, $mode = 2)
    {
        if (is_null($g)) {
            $color = (new ColorBag)->make($r);
            $r = $color->red;
            $g = $color->green;
            $b = $color->blue;
        }

        foreach (['r', 'g', 'b'] as $param) {
            $$param = $this->sanitize($$param);
        }

        $this->red = $r;
        $this->green = $g;
        $this->blue = $b;
        $this->alpha = $a;
        $this->mode = $mode;
    }

    /**
     * Get properties
     *
     * @param string $property The property to get
     *
     * @return mixed
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Get the string representation of the color as a hex value
     *
     * @return string
     */
    public function hex()
    {
        return sprintf(
            '#%s%s%s',
            strtoupper($this->hexify(round($this->red))),
            strtoupper($this->hexify(round($this->green))),
            strtoupper($this->hexify(round($this->blue)))
        );
    }

    /**
     * Get the string representation of the color as an rgb value
     *
     * @return string
     */
    public function rgb()
    {
        return sprintf(
            'rgb(%d, %d, %d)',
            $this->red,
            $this->green,
            $this->blue
        );
    }

    /**
     * Get the string representation of the color as an rgba value
     *
     * @return string
     */
    public function rgba()
    {
        return sprintf(
            'rgba(%d, %d, %d, %g)',
            $this->red,
            $this->green,
            $this->blue,
            $this->alpha
        );
    }

    /**
     * Get the string representation of the color as an HSL value
     *
     * @return string
     */
    public function hsl()
    {
        $data = $this->hslify();

        return sprintf(
            'hsl(%d, %d%%, %d%%)',
            round($data['h'] * 360),
            round($data['s'] * 100),
            round($data['l'] * 100)
        );
    }

    /**
     * Lighten the color up a bit
     *
     * @param float $change The percent to lighten
     *
     * @return Color
     */
    public function lighten($change = 10.0)
    {
        $percent = $change / 100.0;

        foreach (['red', 'blue', 'green'] as $color) {
            $$color = round($this->$color + ($this->$color * $percent));
        }

        return new self($red, $green, $blue, $this->alpha, $this->mode);
    }

    /**
     * Darken the color up a bit
     *
     * @param float $change The percent to darken
     *
     * @return Color
     */
    public function darken($change = 10.0)
    {
        $percent = 1 - ($change / 100.0);

        foreach (['red', 'blue', 'green'] as $color) {
            $$color = $this->$color * $percent;
        }

        return new self($red, $green, $blue, $this->alpha, $this->mode);
    }

    /**
     * Get a total numerical representation of the color by adding its RGB up
     *
     * @return integer
     */
    public function total()
    {
        return $this->red + $this->green + $this->blue;
    }

    /**
     * Ensure the hex value is two characters
     *
     * @param string $value The value to validate
     *
     * @return string
     */
    protected function hexify($value)
    {
        $hex = dechex($value);
        if (strlen($hex) < 2) {
            $hex = '0'.$hex;
        }

        return $hex;
    }

    /**
     * Get the exact opposite color of the current color
     *
     * @return Color
     */
    public function opposite()
    {
        return $this->compliment();
    }

    /**
     * Get a complimentary color based on the diff
     *
     * @param float $diff The range between the color (0.5 = opposite)
     *
     * @return Color
     */
    public function compliment($diff = 0.5)
    {
        $hsl = $this->hslify();
        $h2 = $hsl['h'] + $diff;

        if ($h2 > 1) {
            $h2 -= 1;
        };

        $hsl['h'] = $h2;
    
        return new Color(sprintf(
            'hsl(%d, %d%%, %d%%)',
            ceil($hsl['h'] * 360),
            $hsl['s'] * 100,
            $hsl['l'] * 100
        ));
    }

    /**
     * Get the HSL values for the current color state
     *
     * @return array
     */
    public function hslify()
    {
        $r = $this->red / 255;
        $g = $this->green / 255;
        $b = $this->blue / 255;


        $min = min($r, $g, $b);
        $max = max($r, $g, $b);

        $delta = $max - $min;

        $l = ($max + $min) / 2;

        if ($delta == 0) {
            $h = 0;
            $s = 0;
        } else {
            if ($l < 0.5) {
                $s = $delta / ($max + $min);
            } else {
                $s = $delta / (2 - $max - $min);
            }

            $deltaR = ((( $max - $r) / 6) + ($delta / 2)) / $delta;
            $deltaG = ((( $max - $g) / 6) + ($delta / 2)) / $delta;
            $deltaB = ((( $max - $b) / 6) + ($delta / 2)) / $delta;

            if ($r == $max) {
                $h = $deltaB - $deltaG;
            } elseif ($g == $max) {
                $h = (1 / 3) + $deltaR - $deltaB;
            } elseif ($b == $max) {
                $h = (2 / 3) + $deltaG - $deltaR;
            }

            if ($h < 0) {
                $h += 1;
            }

            if ($h > 1) {
                $h -= 1;
            }
        }

        return [
            'h' => $h,
            's' => $s,
            'l' => $l,
        ];
    }

    /**
     * Ensure RGB values are not above or below limits
     *
     * @param integer $value An RGB value to be validated
     *
     * @return integer
     */
    protected function sanitize($value)
    {
        if ($value > 255) {
            return 255;
        }

        if ($value < 0) {
            return 0;
        }

        return $value;
    }

    /**
     * Set the alpha of your color to a specific value. You can directly set
     * the alpha property, but calling this method allows chaining.
     *
     * @param float $value The value to set the alpha to
     *
     * @return Color
     */
    public function alpha($value)
    {
        if (is_object($value) || is_array($value)) {
            throw new \InvalidArgumentException(
                'Expecting float, received '.gettype($value)
            );
        }

        if (!is_numeric($value)) {
            throw new \InvalidArgumentException(
                'Expecting float, received '.gettype($value).' ('.$value.')'
            );
        }

        $this->alpha = $value;
        return $this;
    }

    /**
     * Get the name of the color
     *
     * @return string
     */
    public function name()
    {
        $repo = ColorRepo::repo();
        return $repo->name($this->hex());
    }

    /**
     * Get a collection of shades based on the current color
     *
     * @param integer $step The size of the step between shades
     *
     * @return Color[]
     */
    public function shades($step = 10)
    {
        $hsl = $this->hslify();
        $h = $hsl['h'] * 360;
        $s = $hsl['s'] * 100;
        $l = $hsl['l'] * 100;

        $shades = [];
        for ($i = 0; $i <= 100; $i += $step) {
            $lightness = 100 - $i;
            $shades[] = new Color('hsl('.$h.', '.$s.'%, '.$lightness.'%');
        }

        return $shades;
    }

    /**
     * Get the depth range
     *
     * @param integer $step The step of the gradual change
     *
     * @return Color[]
     */
    public function saturations($step = 10)
    {
        $hsl = $this->hslify();
    
        $h = $hsl['h'] * 360;
        $l = $hsl['l'] * 100;

        $saturations = [];
        for ($i = 0; $i <= 100; $i += $step) {
            $saturation = 100 - $i;
            $saturations[] = new Color('hsl('.$h.', '.$saturation.'%, '.$l.'%');
        }

        return $saturations;
    }

    /**
     * Get a collection of the same saturation and lightness
     *
     * @param integer $step The size of the step between hues
     *
     * @return Color[]
     */
    public function hues($step = 18)
    {
        $hsl = $this->hslify();
        
        $h = $hsl['h'] * 360;
        $s = $hsl['s'] * 100;
        $l = $hsl['l'] * 100;

        $hues = [];

        $max = (ceil((360 - $h) / $step)) * $step + $h;
        for ($i = $h; $i <= $max; $i += $step) {
            $hue = $i;
            if ($hue > 359) {
                $hue = 359;
            }

            $hues[] = new Color('hsl('.$hue.', '.$s.'%, '.$l.'%');
        }

        $min = $h - (ceil($h / $step) * $step);
        for ($i = $h - $step; $i >= $min; $i -= $step) {
            $hue = $i;
            if ($hue < 0) {
                $hue = 0;
            }

            $hues[] = new Color('hsl('.$hue.', '.$s.'%, '.$l.'%');
        }

        usort($hues, function ($a, $b) {
            $aHsl = $a->hslify();
            $bHsl = $b->hslify();

            if ($aHsl['h'] == $bHsl['h']) {
                return 0;
            }

            return $aHsl['h'] > $bHsl['h'] ? 1 : -1;
        });

        return $hues;
    }

    /**
     * Add a value to the color values
     *
     * @param integer $red   The amount to add to red (ALL if others are null)
     * @param integer $green The amount to add to green
     * @param integer $blue  The amount to add to blue
     *
     * @return Color
     */
    public function add($red, $green = null, $blue = null)
    {
        if (is_null($green)) {
            $green = $red;
            $blue = $red;
        }

        $this->red += $red;
        $this->green += $green;
        $this->blue += $blue;

        return $this;
    }

    /**
     * Convert the color into a string
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->mode === ColorBag::HEX) {
            return $this->hex();
        }

        if ($this->mode === ColorBag::RGBA) {
            return $this->rgba();
        }

        if ($this->mode === ColorBag::RGB) {
            return $this->rgb();
        }

        if ($this->mode === ColorBag::HSL) {
            return $this->hsl();
        }

        return $this->hex();
    }
}
