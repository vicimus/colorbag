<?php

namespace Vicimus\ColorBag;

/**
 * Get colors from an awesome bag filled with magic
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class ColorBag
{
    /**
     * Indicates to return the color as a hex value
     *
     * @var integer
     */
    const HEX = 1;

    /**
     * Indicates to return the color as an rgba value
     *
     * @var integer
     */
    const RGBA = 2;

    /**
     * Indicates to return the color as an rgb value
     *
     * @var integer
     */
    const RGB = 3;

    /**
     *Indicates to return the color as an HSL value
     *
     * @var integer
     */
    const HSL = 4;

    /**
     * Represents RED
     *
     * @var integer
     */
    const RED = 1;

    /**
     * Represents GREEN
     *
     * @var integer
     */
    const GREEN = 2;

    /**
     * Represents BLUE
     *
     * @var integer
     */
    const BLUE = 3;

    /**
     * Default formatted mode is RGBA
     *
     * @var integer
     */
    protected $mode = null;

    /**
     * All colors must be unique
     *
     * @var boolean
     */
    protected $unique = true;

    /**
     * The number of times it should try to grab a unique color matching your
     * defined threshold before aborting and taking the color anyway.
     *
     * @var integer
     */
    protected $abort = 100;

    /**
     * Keeps track of the number of generation attempts to avoid infinite loops
     *
     * @var integer
     */
    protected $attempts = 0;

    /**
     * All colors can be within this value of each other to be considered unique
     *
     * @var integer
     */
    protected $uniqueThreshold = 1;
    
    /**
     * Default options for the bag
     *
     * @var string[]
     */
    protected $defaults = [
        'mode'            => 2,
        'unique'          => true,
        'uniqueThreshold' => 1,
        'abort'           => 10,
    ];

    /**
     * We need to keep track of what colors have been generated to avoid
     * duplicates.
     *
     * @var Color[]
     */
    protected $generated = [];

    /**
     * You can optionally set some defaults via the constructor
     *
     * @param mixed[] $params OPTIONAL parameters
     */
    public function __construct(array $params = array())
    {
        $options = array_merge($this->defaults, $params);
        foreach ($options as $property => $value) {
            if (!array_key_exists($property, $this->defaults)) {
                throw new \InvalidArgumentException(
                    'Invalid parameter: '.$property
                );
            }

            $this->$property = $value;
        }
    }

    /**
     * Grab a color from the bag
     *
     * @param integer $mode The mode to use when formatting the response
     *
     * @return string
     */
    public function grab($mode = null)
    {
        if (is_null($mode)) {
            $mode = $this->mode;
        }

        do {
            $r = rand(0, 255);
            $g = rand(0, 255);
            $b = rand(0, 255);

            $color = new Color($r, $g, $b, 1, $mode);
        } while ($this->exists($color));

        if ($this->unique) {
            $this->generated[] = $color;
        }

        return $color;
    }

    /**
     * Make a color
     *
     * @param integer $red   The red value
     * @param integer $green The green value
     * @param integer $blue  The blue value
     * @param integer $alpha The alpha value
     *
     * @return Color
     */
    public function make($red, $green = null, $blue = null, $alpha = 1)
    {
        if (is_null($green) && substr($red, 0, 1) === '#') {
            return $this->fromHex($red);
        }

        if (is_null($green) && substr($red, 0, 3) === 'rgb') {
            return $this->fromRGB($red);
        }

        if (is_null($green) && substr($red, 0, 3) === 'hsl') {
            return $this->fromHSL($red);
        }

        if (is_null($green)) {
            return $this->fromName($red);
        }

        if (is_null($green)) {
            throw new \InvalidArgumentException(
                'Argument must be hex or rgb string or rgb values'
            );
        }

        return new Color($red, $green, $blue, $alpha);
    }

    /**
     * Create a color from name
     *
     * @param string $color The name of the color
     *
     * @return Color
     */
    public function fromName($color)
    {
        $repo = ColorRepo::repo();
        return $repo->make($color);
    }

    /**
     * Create a color from a Hex string
     *
     * @param string $color The hex string (#FFF or #FFFFFF)
     *
     * @return Color
     */
    public function fromHex($color)
    {
        if (strlen($color) !== 7 && strlen($color) !== 4) {
            throw new \InvalidArgumentException(
                'Color must contain # and either 3 or 6 hex values'
            );
        }

        $color = substr($color, 1);

        $chunk = 2;
        if (strlen($color) === 3) {
            $chunk = 1;
        }

        $position = 0;
        foreach (['red', 'green', 'blue'] as $element) {
            $$element = substr($color, $position, $chunk);
            if ($chunk === 1) {
                $$element .= $$element;
            }

            $$element = hexdec($$element);
            $position += $chunk;
        }

        return new Color($red, $green, $blue);
    }

    /**
     * Create a color from an rgb string
     *
     * @param string $color The rgb string (rgb(0,0,0) or rgba(0,0,0,1))
     *
     * @return Color
     */
    public function fromRGB($color)
    {
        $original = $color;

        if (substr($color, 0, 4) === 'rgba') {
            $type = 'rgba';
        } elseif (substr($color, 0, 3) === 'rgb') {
            $type = 'rgb';
        } else {
            throw new \InvalidArgumentException(
                'Color must be rgb or rgba value'
            );
        }

        $color = str_replace($type, '', $color);
        $color = str_replace('(', '', $color);
        $color = str_replace(')', '', $color);

        $elements = explode(',', $color);

        if (count($elements) < 3) {
            throw new \InvalidArgumentException(
                'Invalid rgb or rgba string supplied: '.$original
            );
        }

        $alpha = count($elements) == 4 ? $elements[3] : 1;

        return new Color($elements[0], $elements[1], $elements[2], $alpha);
    }

    /**
     * Create a color from an hsl string
     *
     * @param string $h The HSL string (hsl(120, 100, 50)) OR the value of H
     * @param string $s OPTIONAL The value of s
     * @param string $l OPTIONAL The value of l
     *
     * @return Color
     */
    public function fromHSL($h, $s = 100, $l = 50)
    {
        $original = $h;

        if (!is_numeric($h)) {
            $h = str_replace('hsl', '', $h);
            $h = str_replace('(', '', $h);
            $h = str_replace(')', '', $h);

            $elements = explode(',', $h);

            $h = (float) $elements[0] / 360;
            $s = trim(str_replace('%', '', $elements[1])) / 100.0;
            $l = trim(str_replace('%', '', $elements[2])) / 100.0;
        } else {
            $h = (float) $h / 360;
            $s = $s / 100.0;
            $l = $l / 100.0;
        }

        if ($s == 0) {
            $r = $l * 255;
            $g = $l * 255;
            $b = $l * 255;
        } else {
            if ($l < 0.5) {
                $var2 = $l * ( 1.0 + $s);
            } else {
                $var2 = ($l + $s) - ($s * $l);
            }

            $var1 = 2.0 * $l - $var2;
                
            $r = 255 * $this->hueToRGB($var1, $var2, $h + (1 / 3));
            $g = 255 * $this->hueToRGB($var1, $var2, $h);
            $b = 255 * $this->hueToRGB($var1, $var2, $h - (1 / 3));
        }

        return new Color($r, $g, $b);
    }

    /**
     * Convert hue to RGB
     *
     * @param integer $v1 V1 value
     * @param integer $v2 V2 value
     * @param integer $vh VH value
     *
     * @return integer
     */
    protected function hueToRGB($v1, $v2, $vh)
    {
        if ($vh < 0) {
            $vh += 1;
        }

        if ($vh > 1) {
            $vh -= 1;
        }

        if ((6 * $vh) < 1) {
            return ($v1 + ($v2 - $v1) * 6 * $vh);
        }

        if ((2 * $vh) < 1) {
            return $v2;
        }

        if ((3 * $vh) < 2) {
            return ($v1 + ($v2 - $v1) * ((2 / 3) - $vh) * 6);
        }

        return $v1;
    }

    /**
     * Grab a handful of colors all at once
     *
     * @param integer $number The number of colors to get
     * @param integer $mode   OPTIONAL specify a mode
     *
     * @return string[]
     */
    public function handful($number, $mode = null)
    {
        if (is_null($mode)) {
            $mode = $this->mode;
        }

        $colors = [];
        for ($i = 0; $i < $number; $i++) {
            $colors[] = $this->grab($mode);
        }

        return $colors;
    }

    /**
     * Get a mainly Red color
     *
     * @return Color
     */
    public function redish()
    {
        return $this->tinted(self::RED);
    }

    /**
     * Get a mainly Blue color
     *
     * @return Color
     */
    public function bluish()
    {
        return $this->tinted(self::BLUE);
    }

    /**
     * Get a mainly Green color
     *
     * @return Color
     */
    public function greenish()
    {
        return $this->tinted(self::GREEN);
    }

    /**
     * Get a color with one of the colors higher than the others so its tinted
     * that color.
     *
     * @param string  $type Indicate which color to tink. 1, 2 or 3 (r,g or b)
     * @param integer $max  The highest color the non-selected colors can be
     *
     * @return Color
     */
    protected function tinted($type, $max = 128)
    {
        $primary = rand($max, 255);
        $secondary = rand(0, $max);

        if ($type === self::RED) {
            $color = new Color($primary, $secondary, $secondary);
            
            if ($this->exists($color)) {
                return $this->redish();
            }
        }

        if ($type === self::GREEN) {
            $color = new Color($secondary, $primary, $secondary);
            
            if ($this->exists($color)) {
                return $this->greenish();
            }
        }

        if ($type === self::BLUE) {
            $color = new Color($secondary, $secondary, $primary);
            
            if ($this->exists($color)) {
                return $this->bluish();
            }
        }

        if (!isset($color)) {
            throw new \InvalidArgumentException(
                '$color value was invalid: '.$type
            );
        }

        if ($this->unique) {
            $this->generated[] = $color;
        }

        return $color;
    }

    /**
     * Checks if a color has already been generated
     *
     * @param Color $color The color to validate
     *
     * @return boolean
     */
    protected function exists(Color $color)
    {
        if (!$this->unique) {
            return false;
        }

        if ($this->attempts > $this->abort) {
            return false;
        }

        $this->attempts++;

        if ($this->uniqueThreshold === 1) {
            $response = in_array($color, $this->generated);
            if (!$response) {
                $this->attempts = 0;
            }

            return $response;
        }

        foreach ($this->generated as $inspect) {
            $matches = 0;
            $thresh = $this->uniqueThreshold;

            if (abs($inspect->red - $color->red) <= $thresh) {
                $matches++;
            }

            if (abs($inspect->blue - $color->blue) <= $thresh) {
                $matches++;
            }

            if (abs($inspect->green - $color->green) <= $thresh) {
                $matches++;
            }

            if ($matches >= 2) {
                return true;
            }
        }

        $this->attempts = 0;
        return false;
    }

    /**
     * Get a spread of colors from lighter to darker
     *
     * @param Color   $color The color to base the spread on
     * @param integer $step  The percent of each step
     * @param integer $up    The number of steps up
     * @param integer $down  THe number of steps down
     *
     * @return Color[]
     */
    public function spread(Color $color, $step = 5, $up = 5, $down = 5)
    {
        $spread = [];

        for ($i = $down; $i >= 1; $i--) {
            $spread[] = $color->darken($step * $i);
        }

        $spread[] = $color;

        for ($i = 1; $i <= $up; $i++) {
            $spread[] = $color->lighten($step * $i);
        }

        return $spread;
    }

    /**
     * Get a pallet of colors that compliment each other
     *
     * @param integer $number The number of colors to get
     * @param Color   $color  The color to base the pallet off of
     *
     * @return Color[]
     */
    public function pallet($number = 3, Color $color = null)
    {
        if (is_null($color)) {
            $color = $this->grab();
        }
        
        $pallet = [$color];

        for ($i = 1; $i < $number; $i++) {
            $color = $color->compliment(1/$number);
            $pallet[] = $color;
        }

        return $pallet;
    }
}
