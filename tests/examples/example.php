<?php

namespace Vicimus\ColorBag;

require_once __DIR__.'/../../vendor/autoload.php';

$bag = new ColorBag;

$color = new Color('#FF0000');

$shades = $color->shades();
$hues = $color->hues();
?>
<div style="display: inline-block; width: 64px; height: 64px; background: <?=$color?>"></div>

<div style="clear: both">
    <h4>Shades</h4>
    <?php foreach ($shades as $shade) : ?>
        <div style="width: 192px;
                    height: 32px;
                    background: <?=$shade?>;
                    border-top: 1px solid #ccc;
                    border-left: 1px solid #ccc;
                    border-right: 1px solid #ccc;
                    box-sizing: border-box;
                    padding-top: 8px;">
            <div style="text-align: center; color: #FFF; text-shadow: 0px 0px 3px #000">
                <?=$shade->hsl()?>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<div style="clear: both">
    <h4>Hues</h4>
    <?php foreach ($hues as $shade) : ?>
        <div style="width: 192px;
                    height: 32px;
                    background: <?=$shade?>;
                    border-top: 1px solid #ccc;
                    border-left: 1px solid #ccc;
                    border-right: 1px solid #ccc;
                    box-sizing: border-box;
                    padding-top: 8px;">
            <div style="text-align: center; color: #FFF; text-shadow: 0px 0px 3px #000">
                <?=$shade->hsl()?>
            </div>
        </div>
    <?php endforeach; ?>
</div>