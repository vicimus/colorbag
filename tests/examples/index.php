<?php

namespace Vicimus\ColorBag;

require_once __DIR__.'/../../vendor/autoload.php';

$colorbag = new ColorBag;

$colors = $colorbag->pallet(6);
$color = $colorbag->bluish();

$shades = $color->shades();
$hues = $color->hues();
$saturations = $color->saturations();

?>
<div>
<h4>6 Pallet (Random)</h4>
<?php foreach ($colors as $color) : ?>
<div style="display: inline-block; width: 64px; height: 64px; background: <?=$color?>"></div>
<?php endforeach; ?>
</div>

<div style="float: left">
<h4>HSL From 0 to 360</h4>
<?php for ($i = 0; $i < 360; $i++) : ?>
<div style="width: 64px; height: 3px; background: <?=$colorbag->fromHSL($i)?>"></div>
<?php endfor; ?>
</div>

<div style="float: left">
<h4>HSL From 0 to 360 at 60%s and 70%l</h4>
<?php for ($i = 0; $i < 360; $i++) : ?>
<div style="width: 64px; height: 3px; background: <?=$colorbag->fromHSL($i, 60, 70)?>"></div>
<?php endfor; ?>
</div>

<div style="clear: both">
    <h4>Shades</h4>
    <?php foreach ($shades as $shade) : ?>
        <div style="width: 192px;
                    height: 32px;
                    background: <?=$shade?>;
                    border-top: 1px solid #ccc;
                    border-left: 1px solid #ccc;
                    border-right: 1px solid #ccc;
                    box-sizing: border-box;
                    padding-top: 8px;">
            <div style="text-align: center; color: #FFF; text-shadow: 0px 0px 3px #000">
                <?=$shade->hsl()?>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<div style="clear: both">
    <h4>Hues</h4>
    <?php foreach ($hues as $hue) : ?>
        <div style="width: 192px;
                    height: 32px;
                    background: <?=$hue?>;
                    border-top: 1px solid #ccc;
                    border-left: 1px solid #ccc;
                    border-right: 1px solid #ccc;
                    box-sizing: border-box;
                    padding-top: 8px;">
            <div style="text-align: center; color: #FFF; text-shadow: 0px 0px 3px #000">
                <?=$hue->hsl()?>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<div style="clear: both">
    <h4>Saturation</h4>
    <?php foreach ($saturations as $saturation) : ?>
        <div style="width: 192px;
                    height: 32px;
                    background: <?=$saturation?>;
                    border-top: 1px solid #ccc;
                    border-left: 1px solid #ccc;
                    border-right: 1px solid #ccc;
                    box-sizing: border-box;
                    padding-top: 8px;">
            <div style="text-align: center; color: #FFF; text-shadow: 0px 0px 3px #000">
                <?=$saturation->hsl()?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
