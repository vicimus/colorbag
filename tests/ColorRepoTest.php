<?php

namespace Vicimus\ColorBag;

/**
 * Test ColorRepo functionality
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class ColorRepoTest extends TestCase
{
    /**
     * Test creating a color from a hex
     *
     * @return void
     */
    public function testHexConstructor()
    {
        $repo = ColorRepo::repo();
        $this->assertInstanceOf(ColorRepo::class, $repo);
    }

    /**
     * Test getting a name for a hex
     *
     * @return void
     */
    public function testColorName()
    {
        $repo = ColorRepo::repo();

        $this->assertEquals('black', $repo->name('#000000'));
    }

    /**
     * Test getting a name from a color object
     *
     * @return void
     */
    public function testColorNameFromObject()
    {
        $color = new Color('#FF0000');
        $rgb = new Color(255, 0, 0);
        $this->assertEquals('red', $color->name());
        $this->assertEquals('red', $rgb->name());
    }

    /**
     * Test creating a color from a name
     *
     * @return void
     */
    public function testColorFromNameViaRepo()
    {
        $repo = ColorRepo::repo();

        $color = $repo->make('Light Slate Gray');
        $this->assertEquals('#6D7B8D', $color->hex());
    }

    /**
     * Test creating a color from a name
     *
     * @return void
     */
    public function testColorFromName()
    {
        $color = new Color('Light Slate Gray');
        $this->assertEquals('#6D7B8D', $color->hex());
    }

    /**
     * Test creating a color with a garbage name
     *
     * @expectedException \InvalidArgumentException
     *
     * @return void
     */
    public function testColorFromGarbageName()
    {
        $color = new Color('Garbage');
    }
}
