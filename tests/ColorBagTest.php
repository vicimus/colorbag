<?php

namespace Vicimus\ColorBag;

/**
 * Main test file to test color bag functions
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class ColorBagTest extends TestCase
{
    /**
     * Tests involving loops should iterate this many times
     *
     * @var integer
     */
    const REPEAT = 1000;

    /**
     * Test the constructor
     *
     * @return void
     */
    public function testConstructor()
    {
        $bag = new ColorBag;
        $this->assertInstanceOf(ColorBag::class, $bag);
    }

    /**
     * Test the grab
     *
     * @return void
     */
    public function testGrab()
    {
        $bag = new ColorBag;
        $color = $bag->grab();
        $this->assertInstanceOf(Color::class, $color);
    }

    /**
     * Grab an rgba color
     *
     * @return void
     */
    public function testGrabRGBA()
    {
        $bag = new ColorBag;
        $color = $bag->grab();
        $this->assertNotFalse(stripos($color, 'rgba'));
    }

    /**
     * Grab a hex color
     *
     * @return void
     */
    public function testGrabHex()
    {
        $bag = new ColorBag;
        $color = $bag->grab(ColorBag::HEX);
        $this->assertNotFalse(stripos($color, '#'));
    }

    /**
     * Grab a hex color via setting the mode in the constructor
     *
     * @return void
     */
    public function testGrabHexByMode()
    {
        $bag = new ColorBag([
            'mode' => ColorBag::HEX,
        ]);

        $color = $bag->grab();
        $this->assertNotFalse(stripos($color, '#'));
    }

    /**
     * Grab multiple colors at once
     *
     * @return void
     */
    public function testGrabMultiple()
    {
        $bag = new ColorBag;

        $colors = $bag->handful(5);
        $this->assertEquals(5, count($colors));
    }

    /**
     * Get a blueish color
     *
     * @return void
     */
    public function testGrabLighten()
    {
        $bag = new ColorBag;
        $color = $bag->grab();

        $lighter = $color->lighten();
        $this->assertTrue($lighter->total() > $color->total());
    }

    /**
     * Get a blueish color
     *
     * @return void
     */
    public function testGrabDarken()
    {
        $bag = new ColorBag;
        $color = $bag->grab();

        $darker = $color->darken();
        $this->assertTrue($darker->total() < $color->total());
    }

    /**
     * Get a handful and verify no values over 255 or under 0
     *
     * @return void
     */
    public function testLimits()
    {
        $bag = new ColorBag;
        $colors = $bag->handful(self::REPEAT);

        foreach ($colors as $color) {
            $this->assertTrue($color->red >= 0 && $color->red <= 255);
            $this->assertTrue($color->blue >= 0 && $color->blue <= 255);
            $this->assertTrue($color->green >= 0 && $color->green <= 255);
            $this->assertTrue($color->total() <= 765);
        }
    }

    /**
     * Get a redish color
     *
     * @return void
     */
    public function testRedish()
    {
        $bag = new ColorBag;
        $color = $bag->redish();
        $this->assertTrue($color->red > $color->blue && $color->red > $color->green);
    }

    /**
     * Get a redish color
     *
     * @return void
     */
    public function testBluish()
    {
        $bag = new ColorBag;
        $color = $bag->bluish();
        $this->assertTrue($color->red < $color->blue && $color->blue > $color->green);
    }

    /**
     * Get a redish color
     *
     * @return void
     */
    public function testGreenish()
    {
        $bag = new ColorBag;
        $color = $bag->greenish();

        $this->assertTrue($color->red < $color->green && $color->blue < $color->green);
    }

    /**
     * Get a main color and then lightened and darkend a set number of steps
     *
     * @return void
     */
    public function testColorStep()
    {
        $bag = new ColorBag;
        $color = $bag->make(110, 110, 151);
        $spread = $bag->spread($color);
        $this->assertEquals(11, count($spread));
    }

    /**
     * Ensure no repeat colors occur
     *
     * @return float
     */
    public function testUniqueness()
    {
        $start = microtime(true);

        $bag = new ColorBag;
        $colors = $bag->handful(self::REPEAT);

        $totals = [];
        foreach ($colors as $color) {
            $this->assertFalse(in_array($color->hex(), $totals));
            $totals[] = $color->hex();
        }

        $end = microtime(true);
        $duration = $end - $start;

        return $duration;
    }

    /**
     * Ensure setting unique to false causes quicker grabs due to not
     * verification.
     *
     * @depends testUniqueness
     *
     * @param float $duration The time it took to generate the unique colors
     *
     * @return void
     */
    public function testNoUniqueness($duration)
    {
        $start = microtime(true);

        $bag = new ColorBag([
            'unique' => false,
        ]);

        $colors = $bag->handful(self::REPEAT);

        $end = microtime(true);
        $span = $end - $start;

        $this->assertTrue($duration > $span);
    }

    /**
     * Ensure grabbing a bunch of tinted colors generates only unique colors
     *
     * @return void
     */
    public function testTintedUniqueness()
    {
        $bag = new ColorBag;

        $colors = [];
        for ($i = 0; $i <= self::REPEAT; $i++) {
            $colors[] = $bag->bluish();
        }

        $exists = [];
        foreach ($colors as $color) {
            $this->assertFalse(in_array($color->hex(), $exists));
            $exists[] = $color->hex();
        }
    }

    /**
     * Ensure that unique threshold behaves as expected
     *
     * @return void
     */
    public function testUniqueThreshold()
    {
        $threshold = 10;

        $bag = new ColorBag([
            'uniqueThreshold' => $threshold,
        ]);

        $colors = $bag->handful(10);

        $generated = [];
        foreach ($colors as $color) {
            foreach ($colors as $inspect) {
                if ($color->hex() == $inspect->hex()) {
                    continue;
                }

                $this->assertTrue(
                    abs($color->red - $inspect->red) > $threshold ||
                    abs($color->blue - $inspect->blue) > $threshold ||
                    abs($color->green - $inspect->green) > $threshold
                );
            }
        }
    }

    /**
     * Test the creation of colors from hex
     *
     * @return void
     */
    public function testFromHex()
    {
        $whiteHex = '#FFF';
        $blackHex = '#000000';

        $bag = new ColorBag;

        $white = $bag->make($whiteHex);
        $this->assertEquals(765, $white->total());

        $black = $bag->make($blackHex);
        $this->assertEquals(0, $black->total());
    }

    /**
     * Test the creation of colors from rgb
     *
     * @return void
     */
    public function testFromRGB()
    {
        $whiteRGB = 'rgb(255,255,255)';
        $blackRGB = 'rgb(0, 0, 0)';

        $bag = new ColorBag;

        $white = $bag->make($whiteRGB);
        $black = $bag->make($blackRGB);

        $this->assertEquals(765, $white->total());
        $this->assertEquals(0, $black->total());
    }

    /**
     * Test the creation of colors from rgba
     *
     * @return void
     */
    public function testFromRGBA()
    {
        $whiteRGB = 'rgba(255,255,255, 0.5)';
        $blackRGB = 'rgba(0, 0, 0, 0.33)';

        $bag = new ColorBag;

        $white = $bag->make($whiteRGB);
        $black = $bag->make($blackRGB);

        $this->assertEquals(765, $white->total());
        $this->assertEquals(0.5, $white->alpha);
        $this->assertEquals(0, $black->total());
        $this->assertEquals(0.33, $black->alpha);
    }

    /**
     * Test creation of color from HSL
     *
     * @return void
     */
    public function testFromHSLRed()
    {
        $red = 'hsl(0, 100%, 50%)';

        $bag = new ColorBag;

        $color = $bag->make($red);

        $this->assertEquals(255, $color->red);
        $this->assertEquals(0, $color->blue);
        $this->assertEquals(0, $color->green);
    }

    /**
     * Test creation of color from HSL
     *
     * @return void
     */
    public function testFromHSLGreen()
    {
        $bag = new ColorBag;
        $green = 'hsl(120, 100%, 50%)';

        $color = $bag->make($green);

        $this->assertEquals(0, $color->red);
        $this->assertEquals(0, $color->blue);
        $this->assertEquals(255, $color->green);
    }

    /**
     * Test creation of color from HSL
     *
     * @return void
     */
    public function testFromHSLBlue()
    {
        $bag = new ColorBag;
        $blue = 'hsl(240, 100%, 50%)';

        $color = $bag->make($blue);

        $this->assertEquals(0, $color->red);
        $this->assertEquals(255, $color->blue);
        $this->assertEquals(0, $color->green);
    }
    /**
     * Test the creation of a pallet
     *
     * @return void
     */
    public function testGetPallet()
    {
        $bag = new ColorBag;

        $color = $bag->grab();

        $pallet = $bag->pallet(3, $color);

        $this->assertEquals(3, count($pallet));
    }

    /**
     * Test creating from HSL directly
     *
     * @return void
     */
    public function testFromHSL()
    {
        $bag = new ColorBag;
        $color = $bag->fromHSL(0);

        $this->assertEquals('#FF0000', $color->hex());
        $this->assertEquals('rgb(255, 0, 0)', $color->rgb());
        $this->assertEquals('hsl(0, 100%, 50%)', $color->hsl());
    }

    /**
     * Test that all HSL values work (H specifically)
     *
     * @return void
     */
    public function testFromHSLBlueExplicit()
    {
        $bag = new ColorBag;
        $color = $bag->fromHSL(240);
        
        $this->assertEquals('#0000FF', $color->hex());
        $this->assertEquals('rgb(0, 0, 255)', $color->rgb());
        $this->assertEquals('hsl(240, 100%, 50%)', $color->hsl());
    }
}
