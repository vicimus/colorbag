<?php

namespace Vicimus\ColorBag;

/**
 * Test Color functionality
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class ColorTest extends TestCase
{
    /**
     * Test creating a color from a hex
     *
     * @return void
     */
    public function testHexConstructor()
    {
        $color = new Color('#88CC00');
        $this->assertInstanceOf(Color::class, $color);
        $this->assertEquals('#88CC00', $color->hex());
    }

    /**
     * Test altering the alpha of a color
     *
     * @return void
     */
    public function testAlphaModify()
    {
        $color = new Color(136, 204, 0);
        $this->assertEquals('#88CC00', $color->hex());

        $color->alpha(0.3);

        $this->assertEquals('rgba(136, 204, 0, 0.3)', $color->rgba());
    }

    /**
     * Test sending garbage to alpha
     *
     * @return void
     */
    public function testAlphaModifyInteger()
    {
        $color = new Color(136, 204, 0);

        $color->alpha(1);
    }

    /**
     * Test sending garbage to alpha
     *
     * @return void
     */
    public function testAlphaModifyNumericString()
    {
        $color = new Color(136, 204, 0);

        $color->alpha('0.9');
    }

    /**
     * Test sending garbage to alpha
     *
     * @expectedException \InvalidArgumentException
     *
     * @return void
     */
    public function testAlphaModifyNonNumericString()
    {
        $color = new Color(136, 204, 0);

        $color->alpha('garbage');
    }

    /**
     * Test sending garbage to alpha
     *
     * @expectedException \InvalidArgumentException
     *
     * @return void
     */
    public function testAlphaModifyObject()
    {
        $color = new Color(136, 204, 0);

        $parameter = new \stdClass;
        $color->alpha($parameter);
    }

    /**
     * Test sending garbage to alpha
     *
     * @expectedException \InvalidArgumentException
     *
     * @return void
     */
    public function testAlphaModifyArray()
    {
        $color = new Color(136, 204, 0);

        $color->alpha(['test' => 'testing']);
    }

    /**
     * Test adding a value to a color
     *
     * @return void
     */
    public function testColorAdd()
    {
        $color = new Color(100, 100, 100);

        $color->add(10);

        $this->assertEquals(330, $color->total());
    }

    /**
     * Test adding value to red
     *
     * @return void
     */
    public function testColorAddRed()
    {
        $color = new Color(100, 100, 100);
        $color->add(10, 0, 0);

        $this->assertEquals(110, $color->red);
    }

    /**
     * Test converting to HSL works as intended
     *
     * @return void
     */
    public function testConvertToHSL()
    {
        $color = new Color(255, 0, 0);

        $hsl = $color->hsl();

        $this->assertEquals('hsl(0, 100%, 50%)', $hsl);
    }

    /**
     * Ensure hex value works for yellow
     *
     * @return void
     */
    public function testYellowHex()
    {
        $color = new Color(0, 255, 255);
        $this->assertEquals('#00FFFF', $color->hex());
    }

    /**
     * Ensure getting the opposite color works
     *
     * @return void
     */
    public function testGetOppositeColorRedGreen()
    {
        $color = new Color(255, 0, 0);

        $opposite = $color->opposite();
        $this->assertEquals('#00FFFF', $opposite->hex());
    }

    /**
     * Ensure getting the opposite color works
     *
     * @return void
     */
    public function testGetOppositeColorBlueYellow()
    {
        $color = new Color(0, 0, 255);

        $opposite = $color->opposite();

        $this->assertEquals('#FFFF00', $opposite->hex());
    }

    /**
     * Test getting shades of a color
     *
     * @return void
     */
    public function testColorShades()
    {
        $color = new Color(0, 0, 255);
        $shades = $color->shades();

        $this->assertInternalType('array', $shades);
    }

    /**
     * Test getting hues of a color
     *
     * @return void
     */
    public function testColorHues()
    {
        $color = new Color('#8800CC');
        $hues = $color->hues();

        $this->assertInternalType('array', $hues);

        $match = false;
        foreach ($hues as $hue) {
            if ($hue->hex() == $color->hex()) {
                $match = true;
            }
        }

        $this->assertTrue($match);
    }

    /**
     * Test getting the saturations of a color
     *
     * @return void
     */
    public function testColorSaturations()
    {
        $color = new Color('#8800CC');
        $saturations = $color->saturations();

        $this->assertInternalType('array', $saturations);

        $match = false;
        foreach ($saturations as $saturation) {
            if ($saturation->hex() == $color->hex()) {
                $match = true;
            }
        }

        $this->assertTrue($match);
    }

    /**
     * Test constructing new colors from common color names
     *
     * @return void
     */
    public function testCreatingFromCommonNames()
    {
        $names = [
            'red',
            'Red',
            'RED',
            'blue',
            'yellow',
            'cyan',
            'green',
            'orange',
            'pink',
            'crimson',
            'purple',
            'brown',
            'grey',
            'gray',
            'white',
            'black',
        ];

        foreach ($names as $name) {
            $color = new Color($name);
            $this->assertInstanceOf(Color::class, $color);
        }
    }
}
