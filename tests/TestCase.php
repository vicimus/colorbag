<?php

namespace Vicimus\ColorBag;

/**
 * A helper class to make testing easier
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class TestCase extends \PHPUnit_Framework_TestCase
{
    //
}
